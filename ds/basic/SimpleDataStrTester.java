public class SimpleDataStrTester {

	public static void main(String[] args) {
		System.out.println("Q example");
	    MyArrQ<Integer> intQ = new MyArrQ<>(3);
	    
	    intQ.enqueue(5);
	    intQ.enqueue(7);
	    intQ.enqueue(9);
	    intQ.enqueue(11);
	    intQ.display();
	    intQ.dequeue();
	    intQ.display();
	    intQ.dequeue();
	    intQ.dequeue();
	    intQ.dequeue();
	    intQ.display();
	    System.out.println("Found 2: " + intQ.contains(2));
	    intQ.enqueue(2);
	    intQ.enqueue(4);
	    intQ.enqueue(6);
	    intQ.enqueue(8);
	    intQ.display();
	    System.out.println("Found 2: " + intQ.contains(2));
	    System.out.println("Found 4: " + intQ.contains(4));
	    System.out.println("Found 6: " + intQ.contains(6));
	    System.out.println("Found 8: " + intQ.contains(8));
	    intQ.dequeue();
	    intQ.dequeue();
	    System.out.println("Found 4: " + intQ.contains(4));
	    System.out.println("Found 6: " + intQ.contains(6));
	    System.out.println("Found 8: " + intQ.contains(8));
	    
		System.out.println("\nStack example");
	    MyArrStack<Integer> intStck = new MyArrStack<>(3);
	    
	    intStck.push(5);
	    intStck.push(7);
	    intStck.push(9);
	    intStck.push(11);
	    intStck.display();
	    intStck.pop();
	    intStck.display();
	    intStck.pop();
	    intStck.pop();
	    intStck.display();
	    intStck.pop();
	    intStck.display();
	    System.out.println("Found 2: " + intStck.contains(2));
	    intStck.push(2);
	    intStck.push(4);
	    intStck.push(6);
	    intStck.push(8);
	    intStck.display();
	    System.out.println("Found 2: " + intStck.contains(2));
	    System.out.println("Found 4: " + intStck.contains(4));
	    System.out.println("Found 6: " + intStck.contains(6));
	    System.out.println("Found 8: " + intStck.contains(8));
	    
	    System.out.println("\nLinked-List example");
	    MyLl<Integer> myll = new MyLl<>();
	    myll.add(5);
	    myll.add(2);
	    myll.add(7);
	    myll.display();
	    myll.remove(2);
	    myll.display();
	    myll.remove(5);
	    myll.display();
	    myll.remove(7);
	    myll.display();
	    myll.remove(9);
	    myll.display();
	    
	    System.out.println("\nLinked-ListSentinel example");
	    MyLlSentinel<Integer> myllSent = new MyLlSentinel<>();
	    myllSent.add(5);
	    myllSent.add(2);
	    myllSent.add(7);
	    myllSent.display();
	    myllSent.remove(2);
	    myllSent.display();
	    myllSent.add(9);
	    myllSent.display();
	    myllSent.remove(5);
	    myllSent.display();
	    myllSent.remove(7);
	    myllSent.display();
	    myllSent.remove(9);
	    myllSent.display();
	    
	    System.out.println("\nStack with Linked-List example");
	    MyLlStack<Integer> intStckLl = new MyLlStack<>(3);
	    
	    intStckLl.push(5);
	    intStckLl.push(7);
	    intStckLl.push(9);
	    intStckLl.push(11);
	    intStckLl.display();
	    intStckLl.pop();
	    intStckLl.display();
	    intStckLl.pop();
	    intStckLl.pop();
	    intStckLl.display();
	    intStckLl.pop();
	    intStckLl.display();            
	    System.out.println("Found 2: " + intStckLl.contains(2));
	    intStckLl.push(2);
	    intStckLl.push(4);
	    intStckLl.push(6);
	    intStckLl.push(8);
	    intStckLl.display();
	    System.out.println("Found 2: " + intStckLl.contains(2));
	    System.out.println("Found 4: " + intStckLl.contains(4));
	    System.out.println("Found 6: " + intStckLl.contains(6));
	    System.out.println("Found 8: " + intStckLl.contains(8));
	}

}
