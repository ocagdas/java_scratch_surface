
public class MyArrStack<E> {
    @SuppressWarnings("unchecked")
	public MyArrStack(int capacityIn) {
    	capacity = capacityIn;
        stckArr = (E[]) new  Object[capacity];
        top = 0;
    }
    
    void display() {
    	if (top > 0) {
    		//BIGO: O(n)
    		int i = top - 1;
    		while (i >= 0) {
    			System.out.print(stckArr[i]+ " ");
    			--i;
    		}
    		System.out.println();
    	} else {
    		System.out.println("Empty");
    	}
    }
    
    boolean push(E val) {
    	//BIGO: O(1)
        if (top < capacity) {
            stckArr[top++] = val;
        } else {
            return false;
        }
        
        return true;
    }
    
    E pop() {
        if (0 == top) {
            return null;
        }
        
        //BIGO: O(1)
        E val = stckArr[--top];
        
        return val;
    }
    
    boolean contains(E val) {
    	int i = 0;
    	try {
    		//BIGO: O(n)
	    	while(false == stckArr[i++].equals(val)) {
	    		// the block below would stop when reach to 'top' and
	    		// won't unnecessarily iterate through the rest of array
	    		// however, for arrays that are expected to be close to full
	    		// most of the time, this is unwanted overhead for every operation
				/*
				 * if (i >= top ) { i = -1; break; }
				 */	
	    	}
    	} catch (ArrayIndexOutOfBoundsException e) {
    		// just swallow the exception
    	} finally {
    		if (i > top) {
    			i = -1;
    		} else {
    			--i;
    		}
    	}
	
    	return i >= 0;
    }
    
    boolean empty() {
    	return top == 0;
    }
    
    private E[] stckArr;
    private int top;
    private int capacity;
}
