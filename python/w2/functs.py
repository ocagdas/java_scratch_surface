
from functools import reduce

# standard args, *args, **kwards (keyword args)
# * & ** are the unpacking operators
def some_func(fixed_arg1, fixed_arg2, *args, **kwargs):
    # * unpacks the variadic arg list to a tuple
    # ** unpacks to a dictionary
    print(f'{fixed_arg1}-{fixed_arg2} - {args} - '
          f'{kwargs}')
    
def test_args_kwargs(arg1, arg2, arg3):
    print("arg1:", arg1)
    print("arg2:", arg2)
    print("arg3:", arg3)

def default_val(arg1='Default Value'):
    print(arg1)

def multi_return():
    return 1,2,3,4

def product(*numbers):
    p = reduce(lambda x, y: x * y, numbers)
    return p 

if __name__ == '__main__':
    print(multi_return())
    a, *b, c = multi_return()
    print(f'{a} {b} {c}')
    
    some_func(3, 5, 10, 20, a=3, b=5)
    
    default_val()
    default_val('Non Default Value')
    
    # call a method with standard args by using *args unpacking
    args = ("two", 3, 5)
    test_args_kwargs(*args)
    
    print('')
    # call a method with standard args by using **kwargs unpacking
    kwargs = {"arg3": 3, "arg2": "two", "arg1": 5}
    test_args_kwargs(**kwargs)
    
    primes = [2, 3, 5, 7, 11, 13]
    # unpack the list
    print(product(*primes))
    # 30030
    # pass list as a single argument
    print(product(primes))
    # [2, 3, 5, 7, 11, 13]
