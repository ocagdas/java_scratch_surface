#! /usr/bin/env python3
# importing functions from modules
from showcase_module.menu_presenter import menu_selector_w_auto_indexing,menu_selector
from showcase_module.classy import MyBaseClass, SubClass

def class_demo():
    base_inst = MyBaseClass('txt_base')
    sub_inst = SubClass('txt_sub', 'more_txt_sub')
    
    base_inst.slazzy_print()
    sub_inst.slazzy_print()
    
    SubClass.some_static_int = 23
    sub_inst.some_static_int = 24
    # note how the static class variable is distinct for the Class and objects
    print(f'SubClass.some_static_int: {SubClass.some_static_int}')
    base_inst.slazzy_print()
    sub_inst.slazzy_print()
    
    # also, inherited classes don't share the same static var as the base
    MyBaseClass.some_static_int = 38
    base_inst.slazzy_print()
    sub_inst.slazzy_print()
    
    base_inst.static_method()
    base_inst.class_method()
    sub_inst.class_method()

if __name__ == '__main__':
    print('Welcome to the main exec')
    class_demo()
    

    

    