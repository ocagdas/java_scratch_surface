#!/usr/bin/env python3
# coding: utf-8

import numpy as np
import pandas as pd
#from mlxtend.preprocessing import TransactionEncoder
from apyori import apriori
 
df = pd.read_csv('apriori_raw.csv', header=None)
#print(df)
df.replace(['\{','\}', ' '], ['','',''], regex=True, inplace = True)
dataset = []
for index, row in df.iterrows():
    dataset.append(row[0].split(','))

print(*dataset, sep='\n')

rules = list(apriori(dataset, min_support=0.25, min_confidence=0.5, min_lift=1.1))
#print(*list(rules), sep='\n')

print('--------------------------------------')
print('Rules: ')
for item in rules:
    print('')
    print(','.join(item.items) + ' -> Support:' + str(item.support) + '\n')
    for cur_ord_stat in item.ordered_statistics:
        print("Rule: " + ','.join(cur_ord_stat.items_base) + " -> " + ','.join(cur_ord_stat.items_add))
        print(f'Confidence: {cur_ord_stat.confidence:.2f}')
        print(f'Lift: {cur_ord_stat.lift:.2f}')
        print('')
    print('=====================================')
print('--------------------------------------')
