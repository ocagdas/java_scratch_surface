import grids
import time

class SearchNode:
    def __init__(self, x, y, parent=None):
        self.x = x
        self.y = y
        self.parent = parent
        self.d = 0 if self.parent == None else self.parent.d + 1
        
    def __eq__(self, other):
        if isinstance(other, SearchNode):
            return self.x == other.x and self.y == other.y
        return False

class IDSearch:
    def __init__(self):
        self._init_internals()
    
    def iterativeDeepening(self, problem, debug=False):
        strt = time.time()
        self._init_internals()
 
        ret = 'failure'
        for limit in range(problem.get_max_depth()):
            self.explored.clear()
            start_node = problem.get_initial_node()
            res = self.depthLimitedSearch(start_node, problem, limit)
            if res != 'cutoff':
                ret = res
                break
        
        print(f'depth: {limit} in {time.time() - strt} s')
        print(f'max_frontier_len: {self.max_frontier_length}, '
                f'explored_states: {self.explored_state_count}')
        if True == debug:
            problem.print_end_state(self.explored, res)
        print()
        return ret
    
    def depthLimitedSearch(self, init_node, problem, limit):
        self.frontier.append(init_node)
        
        ret = 'failure'
        while len(self.frontier) > 0:
            cur_node = self.frontier.pop()
            self.explored.append(cur_node)
            
            if problem.goal_test(cur_node):
                return cur_node
            elif cur_node.d < limit:
                children_nodes = problem.expand(cur_node)
                for child_node in children_nodes:
                    if child_node not in self.frontier:
                        if child_node not in self.explored:
                            self._add_to_frontier(child_node)
                        elif self.explored[self.explored.index(child_node)].d > child_node.d:
                            self.explored[self.explored.index(child_node)] = child_node
                            self.frontier.append(child_node)                
            else:
                ret = 'cutoff'
        return ret
    
    def _add_to_frontier(self, node):
        self.frontier.append(node)
        self.explored_state_count += 1
        self.max_frontier_length = max(self.max_frontier_length, len(self.frontier))
    
    def _init_internals(self):
        self.frontier = list()
        self.explored = list()
        self.max_frontier_length = 0
        self.explored_state_count = 0
    
class SearchProblemGird:
    def __init__(self, grid):
        self.grid = grid
        self.y_limit = len(self.grid)
        if type(self.grid[0]) == list:
            self.x_limit = len(self.grid[0])
        else:
            self.x_limit = 1
    
    def get_initial_node(self):
        try:
            coords = [(j,i) for i, k in enumerate(self.grid) for j,v in enumerate(k) if v == 2][0]
        except:
            coords = (-1, -1)
        finally:
            return SearchNode(coords[0], coords[1])
        
    def goal_test(self, search_node):
        if 3 == self.grid[search_node.y][search_node.x]:
            return True
        else:
            return False
        
    def expand(self, node):
        x, y = node.x, node.y
        attempted_actions = [
            ("left", (x - 1, y)),
            ("right", (x + 1, y)),
            ("up", (x, y - 1)),
            ("downn", (x, y + 1))
        ]
        
        children_nodes = list()
        for action, (x ,y) in attempted_actions:
            if 0 <= x < self.x_limit and 0 <=  y < self.y_limit:
                if self.grid[y][x] != 1:
                    children_nodes.append(SearchNode(x, y, parent=node))
                else:
                    # debug place holder
                    k = 0
        return children_nodes
    
    def get_max_depth(self):
        return self.x_limit * self.y_limit
    
    def print_end_state(self, explored, result):
        debug_grid = self.grid.copy()
        for exp_node in explored[1:]:
            debug_grid[exp_node.y][exp_node.x] = '&'
        if isinstance(result, SearchNode):
            print(f'goal_coords: ({result.x}, {result.y})')
            sol_node = result.parent
            sol_len = 0
            while None != sol_node and None != sol_node.parent:
                sol_len += 1
                debug_grid[sol_node.y][sol_node.x] = '*'
                sol_node = sol_node.parent
            debug_grid[result.y][result.x] = 3
            print(f'solution_length: {sol_len}')
        import numpy as np
        print(np.matrix(debug_grid))

if __name__ == '__main__':
    grid_list = [grids.grid1, grids.grid2, grids.grid3,  grids.grid4, grids.grid5]
    
    for grid in grid_list:
        problem = SearchProblemGird(grid)
        id_search = IDSearch()
        result = id_search.iterativeDeepening(problem, debug=True)